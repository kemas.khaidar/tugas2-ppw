from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages

response = {}


def index(request):
    if 'user_login' in request.session:
        return HttpResponseRedirect(reverse('index'))
    else:
        response['connected'] = False #digunakan sebagai penanda kapan header menampilkan tombol logout
        response['name'] = 'Tugas 2 PPW'
        html = 'login/login.html'
        return render(request, html, response)