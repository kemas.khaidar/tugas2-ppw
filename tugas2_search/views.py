from django.shortcuts import render


# Create your views here.
response = {}
def index(request):
	response['nama_identitas'] = request.session['user_login']
	response['login'] = True
	html = 'tugas2_search/tables/search.html'
	return render(request, html, response)
