
from __future__ import unicode_literals
from django.db import models


# Create your models here.
class User(models.Model):
    kode_identitas = models.CharField('Kode Identitas', max_length=20, primary_key=True, default='Kosong')
    name = models.CharField('Nama', max_length=225)
    email = models.EmailField('Email', default='Kosong')
    profileUrl = models.URLField('Profile LinkedIn', default='Kosong')

class KeahlianKu(models.Model):
    keahlian = models.CharField("Kode keahlian", max_length=50)
    level = models.CharField("Level", max_length = 20)
    identitas = models.CharField('Identitas', primary_key=True,max_length=20, default='Kosong')