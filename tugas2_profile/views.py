from django.views.decorators.csrf import csrf_exempt

from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, JsonResponse
from django.urls import reverse
from django.contrib import messages
from .models import User, KeahlianKu

response = {}


def index(request):
    if 'user_login' in request.session:
        response['name'] = 'Tugas 2 PPW'
        html = 'profile/profile.html'
        response['login'] = True
        response['kode_identitas'] = request.session['kode_identitas']
        exist = User.objects.filter(kode_identitas=request.session['kode_identitas']).exists()
        if not exist:
        	user = User(name='kosong', email='kosong', profileUrl='kosong', kode_identitas=request.session['kode_identitas'])
        	user_id = request.session['kode_identitas']
        	user.save()
        response['jumlahKosong'] = set_data_user(request)
        response['nama_identitas'] = request.session['user_login']
        return render(request, html, response)
    else:
        response['connected'] = False #digunakan sebagai penanda kapan header menampilkan tombol logout
        response['name'] = 'Tugas 2 PPW'
        response['login'] = False
        return redirect("/mahasiswa/login/")

def edit_profile(request):
    if 'user_login' in request.session:
        response['name'] = 'Tugas 2 PPW'
        response['list_keahlian'] = get_keahlian_dari_yang_login(request)
        html = 'profile/edit.html'
        return render(request, html, response)
    else:
        response['connected'] = False #digunakan sebagai penanda kapan header menampilkan tombol logout
        response['name'] = 'Tugas 2 PPW'
        response['login'] = False
        html = 'login/login.html'
        return render(request, html, response)

def get_keahlian_dari_yang_login(request):
	res = []
	keahlian_pengguna = KeahlianKu.objects.all()

	for keahlian in keahlian_pengguna:
		res.append({'keahlian':keahlian.keahlian,'level':keahlian.level})

	return res

@csrf_exempt
def save_database(request):
    if(request.method == 'POST'):
        name = request.POST['firstName'] + ' ' + request.POST['lastName']
        email = request.POST['email']
        profileUrl = request.POST['profileUrl']
        kode_identitas = request.session['kode_identitas']
        user = User(name=name, email=email, profileUrl=profileUrl, kode_identitas=kode_identitas)
        user.save()
        return JsonResponse({'nama':name, 'email':email, 'profileUrl': profileUrl})

def set_data_user(request):
	counterKosong = 0
	user = User.objects.get(kode_identitas=request.session['kode_identitas'])
	if user.name == 'kosong':
		counterKosong = 3
	response['userName'] = user.name
	response['userEmail'] = user.email
	response['userProfileLinkedIn'] = user.profileUrl
	expertiseExist = KeahlianKu.objects.filter(identitas=request.session['kode_identitas']).exists()
	if expertiseExist == 0:
		counterKosong += 1
		response['userKeahlian'] = 'kosong'
	else:
		response['userKeahlian'] = get_keahlian_dari_yang_login(request)
	return counterKosong

@csrf_exempt
def add_keahlian(request):
	if(request.method == 'POST'):
		skills = ['Java','C#','Python','Erlang','Kotlin']
		levels = ['Beginner','Intermediate', 'Expert','Legend']

		skill_taken = skills[int(request.POST['id_skill'])]
		level_of_skill = levels[int(request.POST['id_level'])]
		exist = KeahlianKu.objects.filter(identitas=request.session['kode_identitas'],keahlian=skill_taken).exists()
		
		if exist:
			KeahlianKu.objects.filter(identitas=request.session['kode_identitas'],keahlian=skill_taken).delete()

		objek_keahlian = KeahlianKu()
		objek_keahlian.identitas = request.session['kode_identitas']
		objek_keahlian.keahlian = skill_taken
		objek_keahlian.level = level_of_skill
		objek_keahlian.save()

		return JsonResponse({'identitas':objek_keahlian.identitas, 'skill':skill_taken, 'level':level_of_skill})