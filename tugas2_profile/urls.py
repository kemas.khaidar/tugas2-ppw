from django.conf.urls import url
from .views import index, edit_profile, save_database, add_keahlian
#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^edit/$', edit_profile, name='edit_profile'),
    url(r'^save_database/$', save_database, name='save_database'),
    url(r'^add_keahlian/$', add_keahlian , name='add_keahlian'),
]