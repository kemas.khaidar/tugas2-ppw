"""tugas2_ppw URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic.base import RedirectView
import tugas2_loginstatus.urls as tugas2_loginstatus
import tugas2_profile.urls as tugas2_profile
import tugas2_search.urls as tugas2_search
import tugas2_riwayat.urls as tugas2_riwayat

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^mahasiswa/profile/', include(tugas2_profile, namespace='tugas2_profile')),
    url(r'^mahasiswa/login/', include(tugas2_loginstatus,namespace='tugas2_loginstatus')),
    url(r'^mahasiswa/search/', include(tugas2_search,namespace='tugas2_search')),
    url(r'^mahasiswa/riwayat/', include(tugas2_riwayat, namespace='tugas2_riwayat')),
    url(r'^$', RedirectView.as_view(url="/mahasiswa/profile", permanent="true"), name='index'),
]