from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.urls import reverse
import requests

# Create your views here.
response = {}
RIWAYAT_API = 'https://private-e52a5-ppw2017.apiary-mock.com/riwayat'

def index(request):
	if 'user_login' in request.session:
		response['riwayats'] = get_riwayat()
		html = 'tugas2_riwayat/riwayat/riwayat.html'
		# TODO ngambil rahasia dari objek mahasiswa
		response['rahasia'] = False
		response['nama_identitas'] = request.session['user_login']
		response['login'] = True
		return render(request, html, response)
	else:
		response['connected'] = False #digunakan sebagai penanda kapan header menampilkan tombol logout
		response['name'] = 'Tugas 2 PPW'
		response['login'] = False
		return redirect("/mahasiswa/profile/")

def get_riwayat():
	riwayat = requests.get(RIWAYAT_API)
	return riwayat.json()
